#!/bin/bash
#
src=(
	x264	
	gst-plugins-ugly	
	gst-editing-services
 	gst-python
	python3-tornado
	wheel
	cppy
	python3-kiwisolver
	python3-cycler
	#pytz:  gfs already has it	
	python3-dateutil
	python3-numpy
	python3-matplotlib
	gst-devtools	
	pitivi	
)


for i in ${src[@]}; do
	cd source/$i || exit 1
	sh $i.SlackBuild || exit 1
	upgradepkg --install-new --reinstall /tmp/${i}*_snuk.txz || exit 1
	cd ../..	
done
